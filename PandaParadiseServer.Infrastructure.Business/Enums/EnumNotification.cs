﻿namespace PandaParadiseServer.Infrastructure.Business.Enums
{
    public enum EnumNotification
    {
        ReadyToGo = 1,
        CanImproveBuilding = 2
    }
}