﻿namespace PandaParadiseServer.Infrastructure.Business.Enums
{
    public enum EnumServerEvent
    {
        GetUserData = 1,
        Notification = 2,
        UpgradePandaLevel = 3,
    }
}