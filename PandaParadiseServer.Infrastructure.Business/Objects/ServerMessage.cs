﻿using PandaParadiseServer.Infrastructure.Business.Enums;

namespace PandaParadiseServer.Infrastructure.Business.Objects
{
    public class ServerMessage
    {
        public EnumServerEvent Event { get; set; }
        public object? Data { get; set; }
    }
}