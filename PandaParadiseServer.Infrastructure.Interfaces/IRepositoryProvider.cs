﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Interfaces
{
    public interface IRepositoryProvider
    {
        IUserRepository Users { get; }
        IRepositoryEntityBase<Role> Roles { get; }
        ILogRepository Logs { get; }
        IUserPandaRepository UserPandas { get; }
        IPandaLevelRepository PandaLevels { get; }

        Task SaveChangesAsync();
    }
}