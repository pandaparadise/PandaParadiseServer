﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Interfaces
{
    public interface IRepositoryEntityBase<TEntity> where TEntity : class, IIdentifiable
    {
        void Update(TEntity item);
        Task AddAsync(TEntity item);
        Task<TEntity> GetByIdAsync(Guid id);
        Task<TEntity?> FindByIdAsync(Guid id);
        IQueryable<TEntity> SelectById(Guid id);
    }
}