﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Enums;

namespace PandaParadiseServer.Domain.Interfaces
{
    public interface ILogRepository : IRepositoryEntityBase<Log>    
    {
        Task AddAsync(Guid userId, EnumAction action);
        Log CreateDefault(Guid userId, EnumAction action);
    }
}