﻿using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Domain.Interfaces
{
    public interface IPandaLevelRepository : IRepositoryEntityBase<PandaLevel>    
    {
        Task<PandaLevel?> FindNextLevelAsync(Guid levelId);
    }
}