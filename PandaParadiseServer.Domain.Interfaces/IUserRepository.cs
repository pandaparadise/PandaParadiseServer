﻿using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Domain.Interfaces
{
    public interface IUserRepository : IRepositoryEntityBase<User>    
    {
        Task AddAsync(string email, string name, string password);
        IQueryable<User> Select(string email, string password);
    }
}