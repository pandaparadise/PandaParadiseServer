﻿using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Domain.Interfaces
{
    public interface IUserPandaRepository : IRepositoryEntityBase<UserPanda>    
    {
        IQueryable<UserPanda> Select(Guid userId);
    }
}