﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Enums;

namespace PandaParadiseServer.Domain.Interfaces
{
    public interface IUserResourceRepository : IRepositoryEntityBase<UserResource>    
    {
        UserResource CreateDefault(Guid userId, EnumResourceType type);
    }
}