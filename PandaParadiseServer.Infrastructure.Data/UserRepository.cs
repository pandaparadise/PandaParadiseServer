﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class UserRepository : RepositoryEntityBase<User>, IUserRepository
    {
        public UserRepository(RepositoryProvider repProvider) : base(repProvider) { }

        public override Task AddAsync(User item)
        {
            item.Logs ??= new List<Log>();

            item.Resources = new List<UserResource>
            {
                RepProvider.UserResources.CreateDefault(item.Id, EnumResourceType.Money),
                RepProvider.UserResources.CreateDefault(item.Id, EnumResourceType.Experience),
                RepProvider.UserResources.CreateDefault(item.Id, EnumResourceType.Bamboo)
            };

            item.Logs.Insert(0, RepProvider.Logs.CreateDefault(item.Id, EnumAction.Register));

            return base.AddAsync(item);
        }

        public async Task AddAsync(string email, string name, string password)
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                Email = email,
                Name = name,
                Password = password
            };

            await AddAsync(user);
        }

        public IQueryable<User> Select(string email, string password)
        {
            return _dbSet.Where(i => i.Email == email && i.Password == password);
        }
    }
}