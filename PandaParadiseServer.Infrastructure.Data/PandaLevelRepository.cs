﻿using Microsoft.EntityFrameworkCore;
using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Extensions;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class PandaLevelRepository : RepositoryEntityBase<PandaLevel>, IPandaLevelRepository
    {
        public PandaLevelRepository(RepositoryProvider repProvider) : base(repProvider) { }

        public async Task<PandaLevel?> FindNextLevelAsync(Guid levelId)
        {
            var currentLevel = await _dbSet.SelectById(levelId).Select(i => i.Number).FirstOrDefaultAsync();

            return await _dbSet
                .Where(i => i.Number > currentLevel)
                .OrderBy(i => i.Number)
                .FirstOrDefaultAsync();
        }
    }
}