﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Interfaces;
using PandaParadiseServer.Domain.Interfaces;
using PandaParadiseServer.Infrastructure.Interfaces;
using System.Collections;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class RepositoryProvider : IRepositoryProvider
    {
        public PandaServerContext Context { get; }
        private readonly Hashtable _h = new Hashtable();

        public IUserRepository Users => GetRep<UserRepository, User>();
        public IRepositoryEntityBase<Role> Roles => GetRep<Role>();
        public ILogRepository Logs => GetRep<LogRepository, Log>();
        public IUserResourceRepository UserResources => GetRep<UserResourceRepository, UserResource>();
        public IUserPandaRepository UserPandas => GetRep<UserPandaRepository, UserPanda>();
        public IPandaLevelRepository PandaLevels => GetRep<PandaLevelRepository, PandaLevel>();
        public IRepositoryEntityBase<UserBuilding> UserBuildings => GetRep<UserBuilding>();
        public IRepositoryEntityBase<Food> Foods => GetRep<Food>();
        public IRepositoryEntityBase<FoodInstance> FoodInstances => GetRep<FoodInstance>();
        public IRepositoryEntityBase<BuildingLevel> BuildingLevels => GetRep<BuildingLevel>();
        public IRepositoryEntityBase<PandaUpgrade> PandaUpgrades => GetRep<PandaUpgrade>();
        public IRepositoryEntityBase<UserPandaUpgrade> UserPandaUpgrades => GetRep<UserPandaUpgrade>();
        public IRepositoryEntityBase<UserBuildingUpgrade> UserBuildingUpgrades => GetRep<UserBuildingUpgrade>();
        public IRepositoryEntityBase<BuildingUpgrade> BuildingUpgrades => GetRep<BuildingUpgrade>();
        public IRepositoryEntityBase<FoodBuildingLevel> FoodBuildingLevels => GetRep<FoodBuildingLevel>();

        public RepositoryProvider(PandaServerContext context)
        {
            Context = context;
        }

        public IRepositoryEntityBase<TEntity> GetRep<TEntity>() where TEntity : class, IIdentifiable
        {
            return GetRep<RepositoryEntityBase<TEntity>, TEntity>();
        }

        public T GetRep<T, TEntity>() where T : class, IRepositoryEntityBase<TEntity> where TEntity : class, IIdentifiable
        {
            var typeEntityBase = typeof(T);

            var key = typeEntityBase.FullName! + typeof(TEntity).FullName!;

            if (!_h.ContainsKey(key))
            {
                var rep = Activator.CreateInstance(typeEntityBase, this);

                _h[key] = rep;
            }

            return (T)_h[key]!;
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
    }
}