﻿using Microsoft.EntityFrameworkCore;
using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Infrastructure.Data.Configurations;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class PandaServerContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<UserResource> UserResources { get; set; }
        public DbSet<UserBuilding> UserBuildings { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<FoodInstance> FoodInstances { get; set; }
        public DbSet<BuildingLevel> BuildingLevels { get; set; }
        public DbSet<PandaUpgrade> PandaUpgrades { get; set; }
        public DbSet<PandaLevel> PandaLevels { get; set; }
        public DbSet<UserPanda> UserPandas { get; set; }
        public DbSet<UserPandaUpgrade> UserPandaUpgrades { get; set; }
        public DbSet<UserBuildingUpgrade> UserBuildingUpgrades { get; set; }
        public DbSet<BuildingUpgrade> BuildingUpgrades { get; set; }
        public DbSet<FoodBuildingLevel> FoodBuildingLevels { get; set; }

        public PandaServerContext(DbContextOptions<PandaServerContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new LogConfiguration());
            modelBuilder.ApplyConfiguration(new UserResourceConfiguration());
            modelBuilder.ApplyConfiguration(new UserBuildingConfiguration());
            modelBuilder.ApplyConfiguration(new FoodConfiguration());
            modelBuilder.ApplyConfiguration(new FoodInstanceConfiguration());
            modelBuilder.ApplyConfiguration(new BuildingLevelConfiguration());
            modelBuilder.ApplyConfiguration(new UserPandaConfiguration());
            modelBuilder.ApplyConfiguration(new UpgradePandaConfiguration());
            modelBuilder.ApplyConfiguration(new PandaLevelConfiguration());
            modelBuilder.ApplyConfiguration(new UserBuildingUpgradeConfiguration());
            modelBuilder.ApplyConfiguration(new UserPandaUpgradeConfiguration());
            modelBuilder.ApplyConfiguration(new FoodBuildingLevelConfiguration());
            modelBuilder.ApplyConfiguration(new BuildingUpgradeConfiguration());
        }
    }
}