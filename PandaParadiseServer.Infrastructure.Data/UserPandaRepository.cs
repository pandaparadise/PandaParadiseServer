﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class UserPandaRepository : RepositoryEntityBase<UserPanda>, IUserPandaRepository
    {
        public UserPandaRepository(RepositoryProvider repProvider) : base(repProvider) { }

        public IQueryable<UserPanda> Select(Guid userId)
        {
            return _dbSet.Where(i => i.UserId == userId);
        }
    }
}