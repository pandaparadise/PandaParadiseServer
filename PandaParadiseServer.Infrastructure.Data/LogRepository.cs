﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class LogRepository : RepositoryEntityBase<Log>, ILogRepository
    {
        public LogRepository(RepositoryProvider repProvider) : base(repProvider) { }

        public Log CreateDefault(Guid userId, EnumAction action)
        {
            return new Log
            {
                Id = Guid.NewGuid(),
                DateTimeOper = DateTime.UtcNow,
                UserId = userId,
                Action = action,
            };
        }

        public async Task AddAsync(Guid userId, EnumAction action)
        {
            var log = CreateDefault(userId, action);
            
            await base.AddAsync(log);
        }
    }
}