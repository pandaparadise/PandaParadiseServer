﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using PandaParadiseServer.Domain.Core.Interfaces;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class RepositoryEntityBase<TEntity> : IRepositoryEntityBase<TEntity> where TEntity : class, IIdentifiable
    {
        private readonly PandaServerContext _dbContext;
        protected DbSet<TEntity> _dbSet;
        protected RepositoryProvider RepProvider { get; }

        public RepositoryEntityBase(RepositoryProvider repProvider)
        {
            RepProvider = repProvider;
            _dbContext = repProvider.Context;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public virtual async Task AddAsync(TEntity item)
        {
            await _dbSet.AddAsync(item);
        }

        public virtual async Task<TEntity?> FindByIdAsync(Guid id)
        {
            return await _dbSet.FirstOrDefaultAsync(i => i.Id == id);
        } 
        
        public virtual IQueryable<TEntity> SelectById(Guid id)
        {
            return _dbSet.Where(i => i.Id == id);
        }

        public virtual async Task<TEntity> GetByIdAsync(Guid id)
        {
            var res = await FindByIdAsync(id);

            if (res == null) throw new ApplicationException($"Not found entity {nameof(TEntity)} by id = {id}");

            return res!;
        }

        public virtual void Update(TEntity item)
        {
            _dbSet.Attach(item);
            _dbContext.Entry(item).State = EntityState.Modified;
        }
    }
}