﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PandaParadiseServer.Infrastructure.Data.Migrations
{
    /// <inheritdoc />
    public partial class ChangeStructure : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodInstance_Food_FoodId",
                table: "FoodInstance");

            migrationBuilder.DropTable(
                name: "BuildingLevelFood");

            migrationBuilder.DropTable(
                name: "UpgradeBuildingUserBuilding");

            migrationBuilder.DropTable(
                name: "UpgradePandaUserPanda");

            migrationBuilder.DropTable(
                name: "UpgradeBuilding");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Food",
                table: "Food");

            migrationBuilder.DropColumn(
                name: "BambooCount",
                table: "UserBuildings");

            migrationBuilder.DropColumn(
                name: "MoneyCount",
                table: "UserBuildings");

            migrationBuilder.RenameTable(
                name: "Food",
                newName: "Foods");

            migrationBuilder.AddColumn<int>(
                name: "MaxBamboo",
                table: "BuildingLevels",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MaxMoney",
                table: "BuildingLevels",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Foods",
                table: "Foods",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "BuildingUpgrade",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Bamboo = table.Column<int>(type: "int", nullable: false),
                    Money = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingUpgrade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FoodBuildingLevels",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LevelId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FoodId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodBuildingLevels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FoodBuildingLevels_BuildingLevels_LevelId",
                        column: x => x.LevelId,
                        principalTable: "BuildingLevels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FoodBuildingLevels_Foods_FoodId",
                        column: x => x.FoodId,
                        principalTable: "Foods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPandaUpgrades",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserPandaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PandaUpgradeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPandaUpgrades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPandaUpgrades_UpgradePandas_PandaUpgradeId",
                        column: x => x.PandaUpgradeId,
                        principalTable: "UpgradePandas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPandaUpgrades_UserPandas_UserPandaId",
                        column: x => x.UserPandaId,
                        principalTable: "UserPandas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserResources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserResources_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserBuildingUpgrades",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserBuildingId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BuildingUpgradeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBuildingUpgrades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserBuildingUpgrades_BuildingUpgrade_BuildingUpgradeId",
                        column: x => x.BuildingUpgradeId,
                        principalTable: "BuildingUpgrade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBuildingUpgrades_UserBuildings_UserBuildingId",
                        column: x => x.UserBuildingId,
                        principalTable: "UserBuildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FoodBuildingLevels_FoodId",
                table: "FoodBuildingLevels",
                column: "FoodId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodBuildingLevels_LevelId",
                table: "FoodBuildingLevels",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBuildingUpgrades_BuildingUpgradeId",
                table: "UserBuildingUpgrades",
                column: "BuildingUpgradeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBuildingUpgrades_UserBuildingId",
                table: "UserBuildingUpgrades",
                column: "UserBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPandaUpgrades_PandaUpgradeId",
                table: "UserPandaUpgrades",
                column: "PandaUpgradeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPandaUpgrades_UserPandaId",
                table: "UserPandaUpgrades",
                column: "UserPandaId");

            migrationBuilder.CreateIndex(
                name: "IX_UserResources_UserId",
                table: "UserResources",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodInstance_Foods_FoodId",
                table: "FoodInstance",
                column: "FoodId",
                principalTable: "Foods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodInstance_Foods_FoodId",
                table: "FoodInstance");

            migrationBuilder.DropTable(
                name: "FoodBuildingLevels");

            migrationBuilder.DropTable(
                name: "UserBuildingUpgrades");

            migrationBuilder.DropTable(
                name: "UserPandaUpgrades");

            migrationBuilder.DropTable(
                name: "UserResources");

            migrationBuilder.DropTable(
                name: "BuildingUpgrade");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Foods",
                table: "Foods");

            migrationBuilder.DropColumn(
                name: "MaxBamboo",
                table: "BuildingLevels");

            migrationBuilder.DropColumn(
                name: "MaxMoney",
                table: "BuildingLevels");

            migrationBuilder.RenameTable(
                name: "Foods",
                newName: "Food");

            migrationBuilder.AddColumn<int>(
                name: "BambooCount",
                table: "UserBuildings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MoneyCount",
                table: "UserBuildings",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Food",
                table: "Food",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "BuildingLevelFood",
                columns: table => new
                {
                    AvailiableFoodsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LevelsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingLevelFood", x => new { x.AvailiableFoodsId, x.LevelsId });
                    table.ForeignKey(
                        name: "FK_BuildingLevelFood_BuildingLevels_LevelsId",
                        column: x => x.LevelsId,
                        principalTable: "BuildingLevels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingLevelFood_Food_AvailiableFoodsId",
                        column: x => x.AvailiableFoodsId,
                        principalTable: "Food",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeBuilding",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Experience = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeBuilding", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UpgradePandaUserPanda",
                columns: table => new
                {
                    UpgradesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserPandasId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradePandaUserPanda", x => new { x.UpgradesId, x.UserPandasId });
                    table.ForeignKey(
                        name: "FK_UpgradePandaUserPanda_UpgradePandas_UpgradesId",
                        column: x => x.UpgradesId,
                        principalTable: "UpgradePandas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UpgradePandaUserPanda_UserPandas_UserPandasId",
                        column: x => x.UserPandasId,
                        principalTable: "UserPandas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeBuildingUserBuilding",
                columns: table => new
                {
                    UpgradesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserBuildingsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeBuildingUserBuilding", x => new { x.UpgradesId, x.UserBuildingsId });
                    table.ForeignKey(
                        name: "FK_UpgradeBuildingUserBuilding_UpgradeBuilding_UpgradesId",
                        column: x => x.UpgradesId,
                        principalTable: "UpgradeBuilding",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UpgradeBuildingUserBuilding_UserBuildings_UserBuildingsId",
                        column: x => x.UserBuildingsId,
                        principalTable: "UserBuildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingLevelFood_LevelsId",
                table: "BuildingLevelFood",
                column: "LevelsId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeBuildingUserBuilding_UserBuildingsId",
                table: "UpgradeBuildingUserBuilding",
                column: "UserBuildingsId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradePandaUserPanda_UserPandasId",
                table: "UpgradePandaUserPanda",
                column: "UserPandasId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodInstance_Food_FoodId",
                table: "FoodInstance",
                column: "FoodId",
                principalTable: "Food",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
