﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PandaParadiseServer.Infrastructure.Data.Migrations
{
    /// <inheritdoc />
    public partial class InitialPandaTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuildingLevels",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MaxExperience = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingLevels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Food",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Food", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PandaLevels",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Number = table.Column<int>(type: "int", nullable: false),
                    Speed = table.Column<float>(type: "real", nullable: false),
                    BambooMaxCount = table.Column<int>(type: "int", nullable: false),
                    MoneyMaxCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PandaLevels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeBuilding",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Experience = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeBuilding", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UpgradePandas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Speed = table.Column<float>(type: "real", nullable: false),
                    Bamboo = table.Column<int>(type: "int", nullable: false),
                    Money = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradePandas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserBuildings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    LevelId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BambooCount = table.Column<int>(type: "int", nullable: false),
                    MoneyCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBuildings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserBuildings_BuildingLevels_LevelId",
                        column: x => x.LevelId,
                        principalTable: "BuildingLevels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBuildings_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingLevelFood",
                columns: table => new
                {
                    AvailiableFoodsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LevelsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingLevelFood", x => new { x.AvailiableFoodsId, x.LevelsId });
                    table.ForeignKey(
                        name: "FK_BuildingLevelFood_BuildingLevels_LevelsId",
                        column: x => x.LevelsId,
                        principalTable: "BuildingLevels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingLevelFood_Food_AvailiableFoodsId",
                        column: x => x.AvailiableFoodsId,
                        principalTable: "Food",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPandas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LevelId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPandas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPandas_PandaLevels_LevelId",
                        column: x => x.LevelId,
                        principalTable: "PandaLevels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPandas_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FoodInstance",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FoodId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateReady = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserBuildingId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodInstance", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FoodInstance_Food_FoodId",
                        column: x => x.FoodId,
                        principalTable: "Food",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FoodInstance_UserBuildings_UserBuildingId",
                        column: x => x.UserBuildingId,
                        principalTable: "UserBuildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeBuildingUserBuilding",
                columns: table => new
                {
                    UpgradesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserBuildingsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeBuildingUserBuilding", x => new { x.UpgradesId, x.UserBuildingsId });
                    table.ForeignKey(
                        name: "FK_UpgradeBuildingUserBuilding_UpgradeBuilding_UpgradesId",
                        column: x => x.UpgradesId,
                        principalTable: "UpgradeBuilding",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UpgradeBuildingUserBuilding_UserBuildings_UserBuildingsId",
                        column: x => x.UserBuildingsId,
                        principalTable: "UserBuildings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UpgradePandaUserPanda",
                columns: table => new
                {
                    UpgradesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserPandasId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradePandaUserPanda", x => new { x.UpgradesId, x.UserPandasId });
                    table.ForeignKey(
                        name: "FK_UpgradePandaUserPanda_UpgradePandas_UpgradesId",
                        column: x => x.UpgradesId,
                        principalTable: "UpgradePandas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UpgradePandaUserPanda_UserPandas_UserPandasId",
                        column: x => x.UserPandasId,
                        principalTable: "UserPandas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingLevelFood_LevelsId",
                table: "BuildingLevelFood",
                column: "LevelsId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodInstance_FoodId",
                table: "FoodInstance",
                column: "FoodId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodInstance_UserBuildingId",
                table: "FoodInstance",
                column: "UserBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeBuildingUserBuilding_UserBuildingsId",
                table: "UpgradeBuildingUserBuilding",
                column: "UserBuildingsId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradePandaUserPanda_UserPandasId",
                table: "UpgradePandaUserPanda",
                column: "UserPandasId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBuildings_LevelId",
                table: "UserBuildings",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBuildings_UserId",
                table: "UserBuildings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPandas_LevelId",
                table: "UserPandas",
                column: "LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPandas_UserId",
                table: "UserPandas",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildingLevelFood");

            migrationBuilder.DropTable(
                name: "FoodInstance");

            migrationBuilder.DropTable(
                name: "UpgradeBuildingUserBuilding");

            migrationBuilder.DropTable(
                name: "UpgradePandaUserPanda");

            migrationBuilder.DropTable(
                name: "Food");

            migrationBuilder.DropTable(
                name: "UpgradeBuilding");

            migrationBuilder.DropTable(
                name: "UserBuildings");

            migrationBuilder.DropTable(
                name: "UpgradePandas");

            migrationBuilder.DropTable(
                name: "UserPandas");

            migrationBuilder.DropTable(
                name: "BuildingLevels");

            migrationBuilder.DropTable(
                name: "PandaLevels");
        }
    }
}
