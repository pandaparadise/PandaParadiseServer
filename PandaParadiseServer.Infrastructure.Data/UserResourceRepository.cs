﻿using PandaParadiseServer.Domain.Core.Entities;
using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Interfaces;

namespace PandaParadiseServer.Infrastructure.Data
{
    public class UserResourceRepository : RepositoryEntityBase<UserResource>, IUserResourceRepository
    {
        public UserResourceRepository(RepositoryProvider repProvider) : base(repProvider) { }
        public UserResource CreateDefault(Guid userId, EnumResourceType type)
        {
            return new UserResource
            {
                Id = Guid.NewGuid(),
                Type = type,
                UserId = userId,
                Value = 0,
            };
        }
    }
}