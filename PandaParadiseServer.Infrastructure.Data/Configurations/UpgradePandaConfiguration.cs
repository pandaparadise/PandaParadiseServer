﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UpgradePandaConfiguration : IEntityTypeConfiguration<PandaUpgrade>
    {
        public void Configure(EntityTypeBuilder<PandaUpgrade> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Speed).IsRequired();
            builder.Property(i => i.Money).IsRequired();
            builder.Property(i => i.Bamboo).IsRequired();
        }
    }
}