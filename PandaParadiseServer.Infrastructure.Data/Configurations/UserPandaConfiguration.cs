﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UserPandaConfiguration : IEntityTypeConfiguration<UserPanda>
    {
        public void Configure(EntityTypeBuilder<UserPanda> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Experience).IsRequired();
            builder.HasOne(i => i.Level).WithMany(i => i.UserPandas).HasForeignKey(i => i.LevelId);
            builder.HasOne(i => i.User).WithMany(i => i.UserPandas).HasForeignKey(i => i.UserId);
        }
    }
}