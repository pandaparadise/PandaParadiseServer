﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UserBuildingUpgradeConfiguration : IEntityTypeConfiguration<UserBuildingUpgrade>
    {
        public void Configure(EntityTypeBuilder<UserBuildingUpgrade> builder)
        {
            builder.HasKey(i => i.Id);
            builder.HasOne(i => i.UserBuilding).WithMany(i => i.UserBuildingUpgrades).HasForeignKey(i => i.UserBuildingId);
            builder.HasOne(i => i.BuildingUpgrade).WithMany(i => i.UserBuildingUpgrades).HasForeignKey(i => i.BuildingUpgradeId);
        }
    }
}