﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class PandaLevelConfiguration : IEntityTypeConfiguration<PandaLevel>
    {
        public void Configure(EntityTypeBuilder<PandaLevel> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Number).IsRequired();
            builder.Property(i => i.Speed).IsRequired();
            builder.Property(i => i.MoneyMaxCount).IsRequired();
            builder.Property(i => i.Experience).IsRequired();
            builder.Property(i => i.BambooMaxCount).IsRequired();
        }
    }
}