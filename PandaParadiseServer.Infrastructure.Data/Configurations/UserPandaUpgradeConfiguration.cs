﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UserPandaUpgradeConfiguration : IEntityTypeConfiguration<UserPandaUpgrade>
    {
        public void Configure(EntityTypeBuilder<UserPandaUpgrade> builder)
        {
            builder.HasKey(i => i.Id);
            builder.HasOne(i => i.UserPanda).WithMany(i => i.UserPandaUpgrades).HasForeignKey(i => i.UserPandaId);
            builder.HasOne(i => i.PandaUpgrade).WithMany(i => i.UserPandaUpgrades).HasForeignKey(i => i.PandaUpgradeId);
        }
    }
}