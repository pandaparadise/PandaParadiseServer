﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Name).HasMaxLength(100).IsRequired();
            builder.HasAlternateKey(i => i.Name);
            builder.Property(i => i.Password).HasMaxLength(50).IsRequired();
            builder.Property(i => i.Email).HasMaxLength(320).IsRequired();
            builder.HasAlternateKey(i => i.Email);
            builder.HasOne(i => i.Role).WithMany(i => i.Users).HasForeignKey(i => i.RoleId);
        }
    }
}