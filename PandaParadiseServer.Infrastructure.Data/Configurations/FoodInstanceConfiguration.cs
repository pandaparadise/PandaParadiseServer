﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class FoodInstanceConfiguration : IEntityTypeConfiguration<FoodInstance>
    {
        public void Configure(EntityTypeBuilder<FoodInstance> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.DateReady).IsRequired();

            builder.HasOne(i => i.Food).WithMany(i => i.Instances).HasForeignKey(i => i.FoodId);
            builder.HasOne(i => i.UserBuilding).WithMany(i => i.Foods).HasForeignKey(i => i.UserBuildingId);
        }
    }
}