﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class LogConfiguration : IEntityTypeConfiguration<Log>
    {
        public void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.DateTimeOper).IsRequired();
            builder.Property(i => i.Action).IsRequired();
            builder.HasOne(i => i.User).WithMany(i => i.Logs).HasForeignKey(i => i.UserId);
        }
    }
}