﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class BuildingUpgradeConfiguration : IEntityTypeConfiguration<BuildingUpgrade>
    {
        public void Configure(EntityTypeBuilder<BuildingUpgrade> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Money).IsRequired();
            builder.Property(i => i.Bamboo).IsRequired();
        }
    }
}