﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class FoodBuildingLevelConfiguration : IEntityTypeConfiguration<FoodBuildingLevel>
    {
        public void Configure(EntityTypeBuilder<FoodBuildingLevel> builder)
        {
            builder.HasKey(i => i.Id);
            builder.HasOne(i => i.Level).WithMany(i => i.FoodBuildingLevels).HasForeignKey(i => i.LevelId);
            builder.HasOne(i => i.Food).WithMany(i => i.FoodBuildingLevels).HasForeignKey(i => i.FoodId);
        }
    }
}