﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class BuildingLevelConfiguration : IEntityTypeConfiguration<BuildingLevel>
    {
        public void Configure(EntityTypeBuilder<BuildingLevel> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.MaxExperience).IsRequired();
            builder.Property(i => i.MaxMoney).IsRequired();
            builder.Property(i => i.MaxBamboo).IsRequired();
        }
    }
}