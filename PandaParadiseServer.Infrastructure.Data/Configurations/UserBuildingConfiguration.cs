﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UserBuildingConfiguration : IEntityTypeConfiguration<UserBuilding>
    {
        public void Configure(EntityTypeBuilder<UserBuilding> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Type).IsRequired();

            builder.HasOne(i => i.User).WithMany(i => i.Buildings).HasForeignKey(i => i.UserId);
            builder.HasOne(i => i.Level).WithMany(i => i.UserBuildings).HasForeignKey(i => i.LevelId);
        }
    }
}