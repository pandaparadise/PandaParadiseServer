﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PandaParadiseServer.Domain.Core.Entities;

namespace PandaParadiseServer.Infrastructure.Data.Configurations
{
    public class UserResourceConfiguration : IEntityTypeConfiguration<UserResource>
    {
        public void Configure(EntityTypeBuilder<UserResource> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Value).IsRequired();
            builder.Property(i => i.Type).IsRequired();
            builder.HasOne(i => i.User).WithMany(i => i.Resources).HasForeignKey(i => i.UserId);
        }
    }
}