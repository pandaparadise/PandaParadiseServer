﻿using System.ComponentModel.DataAnnotations;

namespace PandaParadiseServer.Models
{
    public class Register
    {
        [EmailAddress(ErrorMessage = "Invalid email"), StringLength(maximumLength: 320)]
        public string Email { get; set; } = null!;

        [StringLength(maximumLength: 100)]
        public string Name { get; set; } = null!;

        [StringLength(maximumLength: 50, MinimumLength = 8)]
        public string Password { get; set; } = null!;
    }
}