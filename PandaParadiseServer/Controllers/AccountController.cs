﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PandaParadiseServer.Infrastructure.Interfaces;
using PandaParadiseServer.Models;
using PandaParadiseServer.Options;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace PandaParadiseServer.Controllers
{
    [Route("account"), ApiController]
    public class AccountController : Controller
    {
        private readonly IRepositoryProvider _repProvider;
        public AccountController(IRepositoryProvider repProvider)
        {
            _repProvider = repProvider;
        }

        [HttpPost, Route("register")]
        public async Task<IActionResult> Register(Register register)
        {
            try
            {
                await _repProvider.Users.AddAsync(register.Email, register.Name, register.Password);
                await _repProvider.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost, Route("token")]
        public async Task<IActionResult> Token(string email, string password)
        {
            var identity = await GetIdentity(email, password);

            if (identity == null) return BadRequest("Invalid username or password");

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                        expires: now.Add(AuthOptions.LifeTime),
                        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var accessToken = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = accessToken,
            };

            return Ok(response);
        }

        private async Task<ClaimsIdentity?> GetIdentity(string email, string password)
        {
            var user = await _repProvider.Users.Select(email, password)
                .Select(i => new
                {
                    i.Id,
                    RoleName = i.Role!.Name!
                })
                .FirstOrDefaultAsync();

            if (user == null) return null;

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id.ToString())
            };

            if (!string.IsNullOrWhiteSpace(user.RoleName))
            {
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, user.RoleName));
            }

            var claimsIdentity = new ClaimsIdentity(
                claims,
                "Token",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }
    }
}