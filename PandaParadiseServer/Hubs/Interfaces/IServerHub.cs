﻿namespace PandaParadiseServer.Hubs.Objects
{
    public interface IServerHub
    {
        Task SendAsync(string message);
    }
}
