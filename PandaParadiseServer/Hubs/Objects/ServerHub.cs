﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Core.Extensions;
using PandaParadiseServer.Infrastructure.Business.Enums;
using PandaParadiseServer.Infrastructure.Business.Objects;
using PandaParadiseServer.Infrastructure.Interfaces;
using System.Text.Json;

namespace PandaParadiseServer.Hubs.Objects
{
    [Authorize]
    public class ServerHub : BaseHub<IServerHub>
    {
        public const string Path = "/server";

        private readonly IRepositoryProvider _repProvider;

        public ServerHub(IRepositoryProvider repProvider)
        {
            _repProvider = repProvider;
        }

        public override async Task OnConnectedAsync()
        {
            await WriteActionAsync(EnumAction.Connect);
            await base.OnConnectedAsync();
        }

        public async Task WriteActionAsync(EnumAction action)
        {
            await _repProvider.Logs.AddAsync(UserId, action);
            await _repProvider.SaveChangesAsync();
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            await WriteActionAsync(EnumAction.Disconnect);
            await base.OnDisconnectedAsync(exception);
        }

        public async Task ServerAsync(string message)
        {
            try
            {
                var serverMessage = JsonSerializer.Deserialize<ServerMessage>(message)!;

                try
                {
                    switch (serverMessage.Event)
                    {
                        case EnumServerEvent.GetUserData: await GetUserDataAsync(); break;
                        case EnumServerEvent.UpgradePandaLevel: await UpgradePandaLevelAsync(serverMessage.Data); break;
                        default: break;
                    }
                }
                catch (Exception ex) 
                {
                    await SendAsync(serverMessage.Event, ex.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task UpgradePandaLevelAsync(object? data)
        {
            var upgradePandaLevel = JsonSerializer.Deserialize<UpgradePandaLevel>(data?.ToString()!)!;

            var panda = await _repProvider.UserPandas
                .Select(UserId)
                .SelectById(upgradePandaLevel.PandaId)
                .FirstOrDefaultAsync();

            if (panda == null) throw new ApplicationException("Panda not found");

            var nextLevel = await _repProvider.PandaLevels.FindNextLevelAsync(panda.LevelId);

            if (nextLevel == null) throw new ApplicationException("Next level not found");

            if (panda.Experience < nextLevel.Experience) throw new ApplicationException("Lack of experience");

            panda.Experience -= nextLevel.Experience;
            panda.LevelId = nextLevel.Id;

            _repProvider.UserPandas.Update(panda);
            await _repProvider.SaveChangesAsync();

            await SendAsync(EnumServerEvent.UpgradePandaLevel);
        }

        private async Task GetUserDataAsync()
        {
            await SendAsync(EnumServerEvent.GetUserData, "Settings");
        }

        public async Task SendAsync(ServerMessage serverMessage)
        {
            var message = JsonSerializer.Serialize(serverMessage);

            await Clients.Caller.SendAsync(message);
        }

        private async Task SendAsync(EnumServerEvent serverEvent, object? data = null)
        {
            var message = new ServerMessage()
            {
                Event = serverEvent,
                Data = data
            };

            await SendAsync(message);
        }
    }
}