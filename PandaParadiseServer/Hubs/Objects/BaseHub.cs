﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace PandaParadiseServer.Hubs.Objects
{
    [Authorize]
    public abstract class BaseHub<T> : Hub<T> where T : class
    {
        private Guid? _userId;
        protected Guid UserId => _userId ??= new Guid(Context.User!.Identity!.Name!);
    }
}