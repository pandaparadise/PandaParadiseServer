﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace PandaParadiseServer.Options
{
    public class AuthOptions
    {
        public const string ISSUER = "PandaParadiseServer";
        public const string AUDIENCE = "PandaParadiseClient";
        const string KEY = "298c5700-ecb3-4f00-8423-0b9aebbcd316";
        public static readonly TimeSpan LifeTime = TimeSpan.FromDays(2);
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}