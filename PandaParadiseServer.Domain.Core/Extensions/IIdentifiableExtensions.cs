﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Extensions
{
    public static class IIdentifiableExtensions
    {
        public static IQueryable<T> SelectById<T>(this IQueryable<T> q, Guid id) where T : class, IIdentifiable
        {
            return q.Where(i => i.Id == id);
        }
    }
}