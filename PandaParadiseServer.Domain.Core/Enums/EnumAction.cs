﻿namespace PandaParadiseServer.Domain.Core.Enums
{
    public enum EnumAction
    {
        Register = 1,
        Connect = 2,
        Disconnect = 3,
        Purchase = 4
    }
}