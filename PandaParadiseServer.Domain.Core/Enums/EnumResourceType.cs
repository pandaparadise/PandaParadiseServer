﻿namespace PandaParadiseServer.Domain.Core.Enums
{
    public enum EnumResourceType
    {
        Bamboo = 1,
        Money = 2,
        Experience = 3
    }
}