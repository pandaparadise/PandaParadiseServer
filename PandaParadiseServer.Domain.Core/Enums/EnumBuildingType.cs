﻿namespace PandaParadiseServer.Domain.Core.Enums
{
    public enum EnumBuildingType
    {
        BambooForests = 1,
        Farms = 2
    }
}