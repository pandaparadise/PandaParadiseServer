﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class UserPanda : IIdentifiable
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid LevelId { get; set; }
        public int Experience { get; set; }
        public PandaLevel Level { get; set; } = null!;
        public User User { get; set; } = null!;
        public List<UserPandaUpgrade>? UserPandaUpgrades { get; set; }
    }
}