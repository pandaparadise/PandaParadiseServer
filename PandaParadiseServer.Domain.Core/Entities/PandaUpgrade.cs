﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class PandaUpgrade : IIdentifiable
    {
        public Guid Id { get; set; }
        public float Speed { get; set; }
        public int Bamboo { get; set; }
        public int Money { get; set; }
        public List<UserPandaUpgrade>? UserPandaUpgrades { get; set; }
    }
}