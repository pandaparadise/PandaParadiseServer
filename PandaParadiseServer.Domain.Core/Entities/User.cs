﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public partial class User : IIdentifiable
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public Guid? RoleId { get; set; }
        public Role? Role { get; set; }
        public List<Log>? Logs { get; set; }
        public List<UserResource> Resources { get; set; } = null!;
        public List<UserBuilding>? Buildings { get; set; }
        public List<UserPanda>? UserPandas { get; set; }
    }
}