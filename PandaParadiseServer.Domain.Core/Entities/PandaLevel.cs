﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class PandaLevel : IIdentifiable
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public float Speed { get; set; }
        public int BambooMaxCount { get; set; }
        public int MoneyMaxCount { get; set; }
        public int Experience { get; set; }
        public List<UserPanda>? UserPandas { get; set; }
    }
}