﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class UserBuildingUpgrade : IIdentifiable
    {
        public Guid Id { get; set; }
        public Guid UserBuildingId { get; set; }
        public Guid BuildingUpgradeId { get; set; }
        public UserBuilding UserBuilding { get; set; } = null!;
        public BuildingUpgrade BuildingUpgrade { get; set; } = null!;
    }
}