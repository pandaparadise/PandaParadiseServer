﻿using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class UserBuilding : IIdentifiable
    {
        public Guid Id { get; set; }
        public EnumBuildingType Type { get; set; }
        public Guid LevelId { get; set; }
        public Guid UserId { get; set; }
        public List<UserBuildingUpgrade>? UserBuildingUpgrades { get; set; }
        public List<FoodInstance>? Foods { get; set; }
        public User User { get; set; } = null!;
        public BuildingLevel Level { get; set; } = null!;
    }
}