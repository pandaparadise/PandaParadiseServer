﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class FoodInstance : IIdentifiable
    {
        public Guid Id { get; set; }
        public Guid FoodId { get; set; }
        public DateTime DateReady { get; set; }
        public Guid UserBuildingId { get; set; }
        public Food Food { get; set; } = null!; 
        public UserBuilding UserBuilding { get; set; } = null!; 
    }
}