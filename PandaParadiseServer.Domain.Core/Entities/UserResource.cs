﻿using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public partial class UserResource : IIdentifiable
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public EnumResourceType Type { get; set; }
        public int Value { get; set; }
        public User User { get; set; } = null!;
    }
}