﻿using PandaParadiseServer.Domain.Core.Enums;
using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public partial class Log : IIdentifiable
    {
        public Guid Id { get; set; }
        public DateTime DateTimeOper { get; set; }
        public Guid UserId { get; set; }
        public EnumAction Action { get; set; }
        public User User { get; set; } = null!;
    }
}