﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class FoodBuildingLevel : IIdentifiable
    {
        public Guid Id { get; set; }
        public Guid LevelId { get; set; }
        public Guid FoodId { get; set; }
        public Food Food { get; set; } = null!; 
        public BuildingLevel Level{ get; set; } = null!; 
    }
}