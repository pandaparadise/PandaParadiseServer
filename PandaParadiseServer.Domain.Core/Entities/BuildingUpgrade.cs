﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class BuildingUpgrade : IIdentifiable
    {
        public Guid Id { get; set; }
        public int Bamboo { get; set; }
        public int Money { get; set; }
        public List<UserBuildingUpgrade>? UserBuildingUpgrades { get; set; }
    }
}