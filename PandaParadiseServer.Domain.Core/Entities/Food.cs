﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class Food : IIdentifiable
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public List<FoodBuildingLevel>? FoodBuildingLevels { get; set; }
        public List<FoodInstance>? Instances { get; set; }
    }
}