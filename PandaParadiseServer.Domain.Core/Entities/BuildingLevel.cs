﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class BuildingLevel : IIdentifiable
    {
        public Guid Id { get; set; }
        public int MaxBamboo { get; set; }
        public int MaxMoney { get; set; }
        public int MaxExperience { get; set; }
        public List<FoodBuildingLevel>? FoodBuildingLevels { get; set; }
        public List<UserBuilding>? UserBuildings { get; set; }
    }
}