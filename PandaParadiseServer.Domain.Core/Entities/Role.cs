﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public partial class Role : IIdentifiable
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public List<User>? Users { get; set; }
    }
}