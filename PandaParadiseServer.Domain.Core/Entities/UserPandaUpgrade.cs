﻿using PandaParadiseServer.Domain.Core.Interfaces;

namespace PandaParadiseServer.Domain.Core.Entities
{
    public class UserPandaUpgrade : IIdentifiable
    {
        public Guid Id { get; set; }
        public Guid UserPandaId { get; set; }
        public Guid PandaUpgradeId { get; set; }
        public UserPanda UserPanda { get; set; } = null!;
        public PandaUpgrade PandaUpgrade { get; set; } = null!;
    }
}