﻿namespace PandaParadiseServer.Domain.Core.Interfaces
{
    public interface IIdentifiable
    {
        Guid Id { get; set; }
    }
}